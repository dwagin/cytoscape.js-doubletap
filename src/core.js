import DoubleTap from './doubletap'

export default function (options) {
  let cy = this
  return new DoubleTap(cy, options)
}
