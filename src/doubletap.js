function DoubleTap (cy, timeout) {
  this.timeout = timeout || 300

  let id
  let before

  cy.on('tap', e => {
    let { target } = e

    if (id && before) {
      clearTimeout(id)
    }

    if (before === target) {
      target.trigger('doubletap')
      before = null
    } else {
      id = setTimeout(() => { before = null }, this.timeout)
      before = target
    }
  })
}

export default DoubleTap
